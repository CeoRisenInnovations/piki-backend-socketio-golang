FROM golang:1.14.4

WORKDIR /usr/src/app

COPY go.mod ./

COPY . .

RUN go build ./...

CMD ["go", "run", "./..."]