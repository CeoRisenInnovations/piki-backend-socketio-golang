package main

import (
	"encoding/json"
	"fmt"
	socketio "github.com/googollee/go-socket.io"
	"net/http"
	"time"
)

type socketServerHandler struct {
	Server socketio.Server
}

func (s *socketServerHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	req, err := request.URL.Query()["status"]
	if !err || len(req[0]) < 1 {
		http.Error(writer, "query error", 404)
	}
	fmt.Println(req)
	if req[0] == "successful" {
		tx, err := request.URL.Query()["transaction_id"]
		if !err || len(req[0]) < 1 {
			http.Error(writer, "query error", 404)
		}
		client := &http.Client{Timeout: 100 * time.Second}
		fmt.Println(tx[0])
		request, e := http.NewRequest("GET", "https://api.flutterwave.com/v3/transactions/"+tx[0]+"/verify", nil)
		if e != nil {
			http.Error(writer, "query error", 404)
		}
		request.Header.Add("Content-Type", "application/json")
		request.Header.Add("Authorization", "Bearer FLWSECK_TEST-3f0d8bc54824d20e7c5a525eaad31fd0-X")

		response, errr := client.Do(request)
		if errr != nil {
			http.Error(writer, "verify error", 404)
		}

		var pv PaymentVerify

		if response != nil {
			defer response.Body.Close()

			v := json.NewDecoder(response.Body).Decode(&pv)
			if v != nil {
				http.Error(writer, v.Error(), http.StatusBadRequest)
			}
			fmt.Fprintf(writer, "Payment Verify: %+v", *pv.Data.Status)
			if *pv.Data.Status != "successful" {
				fmt.Fprintf(writer, "failed: %+v", *pv.Data)
			} else {
				pn := *pv.Data.Customer.PhoneNumber
				fmt.Printf(pn)
				// TODO: -_O_- fix
				// original database design ded not use area codes
				// once it is updated this can be removed
				if substring(pn, 0, 3) == "254" {
					fmt.Printf("0" + substring(pn, 3, len(pn)))
					s.Server.BroadcastToRoom("/", "0"+substring(pn, 3, len(pn)), "paymentVerified", true)
				} else {
					s.Server.BroadcastToRoom("/", pn, "paymentVerified", true)
				}
				fmt.Fprintf(writer, " verified☑︎: %+v", *pv.Data)
			}
		}
	}
}
