package main

import "time"

type RiderLocation struct {
	Latitude   *float64 `json:"latitude"`
	Longitude  *float64 `json:"longitude"`
	RoomNumber *string  `json:"phoneNumber"`
}

type Customer struct {
	ID          *int64     `json:"id"`
	Name        *string    `json:"name"`
	PhoneNumber *string    `json:"phone_number"`
	Email       *string    `json:"email"`
	CreatedAt   *time.Time `json:"created_at"`
}
type VerifyData struct {
	ID                *int64     `json:"id"`
	TxRef             *string    `json:"tx_ref"`
	FlwRef            *string    `json:"flw_ref"`
	DeviceFingerprint *string    `json:"device_fingerprint"`
	Amount            *float64     `json:"amount"`
	Currency          *string    `json:"currency"`
	ChargedAmount     *float64    `json:"charged_amount"`
	AppFee            *float64   `json:"app_fee"`
	MerchantFee       *float64   `json:"merchant_fee"`
	ProcessorResponse *string    `json:"processor_response"`
	AuthModel         *string    `json:"auth_model"`
	IP                *string    `json:"ip"`
	Narration         *string    `json:"narration"`
	Status            *string    `json:"status"`
	PaymentType       *string    `json:"payment_type"`
	CreatedAt         *time.Time `json:"created_at"`
	AccountId         *int64     `json:"account_id"`
	AmountSettled     *float64   `json:"amount_settled"`
	Customer          *Customer  `json:"customer"`
}
type PaymentVerify struct {
	Status  *string     `json:"status"`
	Message *string     `json:"message"`
	Data    *VerifyData `json:"data"`
}
