package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	socketio "github.com/googollee/go-socket.io"
)

func main() {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}

	// /socket.io/ endpoint to listen for connections
	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")

		fmt.Println("connected:" + s.ID())

		return nil
	})
	// socket.io endpoint that listens for errors
	server.OnError("/", func(s socketio.Conn, e error) {
		fmt.Println("socket.io error:", e)
	})
	// socket.io endpoint that listens for disconnected clients
	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		fmt.Println("disconnected:", s.ID(), reason, ": <= done")
		fmt.Println("Number of connections in room: ", server.RoomLen("/", "currentRequests"))
	})
	// TODO: Naming..
	server.OnEvent("/", "current-requests", func(s socketio.Conn, msg string) {
		fmt.Println("join current requests...")
		s.Join("currentRequests")
	})

	server.OnEvent("/", "ride_requested", func(s socketio.Conn, msg string) {
		fmt.Println("ride_requested::socket.io")
		server.BroadcastToRoom("/", "currentRequests", "rideRequested")
	})

	// join-active-ride-room takes a socket request of type string and creates a room
	// if that room already has two connections, the connection is refused
	server.OnEvent("/", "join-active-ride-room", func(s socketio.Conn, msg string) (string, error) {

		if server.RoomLen("/", msg) < 2 {
			fmt.Println(msg)
			fmt.Println("New connection joining room: ", msg)
			s.Join(msg)
			fmt.Println("Number of connections in room: ", server.RoomLen("/", msg))
			return msg, nil
		}

		return "false", errors.New("unavailable")
	})

	server.OnEvent("/", "ride-ready", func(s socketio.Conn, msg string) (bool, error) {
		if server.RoomLen("/", msg) < 2 {
			s.Join(msg)
			fmt.Println(msg)
			server.BroadcastToRoom("/", msg, "event:newConnection", true)

			return true, nil
		}
		return false, errors.New("ride already taken or has cancelled")
	})

	server.OnEvent("/", "ride-arrived", func(s socketio.Conn, msg string) (bool, error) {
		if server.RoomLen("/", msg) == 2 {
			fmt.Println(msg + " ride arrived")
			server.BroadcastToRoom("/", msg, "rideArrived", true)
			return true, nil
		}
		return false, nil
	})

	server.OnEvent("/", "begin-trip", func(s socketio.Conn, msg string) (bool, error) {
		fmt.Println(msg + " 's ride has begun")
		server.BroadcastToRoom("/", msg, "rideStarted", true)
		return true, nil
	})

	server.OnEvent("/", "leave-room", func(s socketio.Conn, msg string) bool {
		s.Leave(msg)

		fmt.Println(s.ID() + " has left the room " + msg)

		//server.ClearRoom("/", msg)
		fmt.Println(server.RoomLen("/", msg))
		return true
	})

	type EndRide struct {
		PhoneNumber *string  `json:"phone_number"`
		Amount      *float64 `json:"amount"`
		RideID      *string  `json:"rideId"`
	}
	type FlutterWaveData struct {
		ID *int `json:"id"`
	}
	type FlutterWaveResponse struct {
		Status  *string          `json:"status"`
		Message *string          `json:"message"`
		Data    *FlutterWaveData `json:"data"`
	}

	server.OnEvent("/", "end-ride", func(s socketio.Conn, msg string) bool {
		data := &EndRide{}

		err := json.Unmarshal([]byte(msg), data)
		if err != nil {
			fmt.Println("OHNO!!!")
			fmt.Println(err)
			return false
		}

		client := &http.Client{Timeout: 500 * time.Second}
		// Begins mpesa transaction for the user on their phone
		req, ohno := http.NewRequest("POST", "https://piki-backend.herokuapp.com/api/v1/mpesa-payment", bytes.NewBuffer([]byte(msg)))
		if ohno != nil {
			fmt.Println("OHNO!!")
			fmt.Println(ohno)
		}
		req.Header.Add("Content-Type", "application/json")

		res, damn := client.Do(req)
		if damn != nil {
			fmt.Println("OHNO")
			fmt.Println(damn)
		}
		if res != nil {
			defer res.Body.Close()

			fmt.Printf("Payment Verify: %+v\n", *data.PhoneNumber)
			body, _ := ioutil.ReadAll(res.Body)
			fmt.Println("response Status:", res.Status)
			if res.StatusCode == 404 {
				return false
			}
			/* fmt.Println(string(body)) */
			paymentData := &FlutterWaveResponse{Data: &FlutterWaveData{}}
			err := json.Unmarshal([]byte(body), paymentData)
			if err != nil {
				fmt.Println("OHNO!")
				fmt.Println(err)
				return false
			}
			fmt.Println(*paymentData.Data.ID)
			server.BroadcastToRoom("/", *data.PhoneNumber, "ride-ended", *paymentData.Data.ID)
			ri := strconv.Itoa(*paymentData.Data.ID)
			fmt.Println(ri)
			// Sends the Ride Id as well as the Payment Id to update the record
			url := "https://piki-backend.herokuapp.com/api/v1/ride/" + *data.RideID + "/" + ri
			fmt.Println(url)
			req, err := http.NewRequest("POST", url, nil)
			if err != nil {
				fmt.Println("UPDATE RIDE PAYMENT")
				fmt.Println(ri)
				fmt.Println(err)
			}
			client.Do(req)
		}
		return true
	})

	server.OnEvent("/", "update-location", func(s socketio.Conn, msg string) (string, error) {
		res := RiderLocation{}
		err := json.Unmarshal([]byte(msg), &res)
		if err != nil {
			fmt.Println(err)
		}

		server.BroadcastToRoom("/", *res.RoomNumber, "riderPosition", []float64{*res.Latitude, *res.Longitude})

		return "server says:::" + msg, nil
	})
	// starts the socket.io server
	go func() {
		s := server.Serve()
		if s != nil {
			log.Fatalln(s.Error())
		}
	}()
	// closes the socket.io server
	defer func() {
		s := server.Close()
		if s != nil {
			fmt.Println(s.Error())
		}
	}()

	var port string

	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
		name, err := os.Hostname()
		if err != nil {
			fmt.Println(err)
		}

		http.Handle("/socket.io/", server)
		http.Handle("/", http.FileServer(http.Dir("./server/asset")))
		http.Handle("/payment", &socketServerHandler{Server: *server})

		log.Println("Serving at " + name + ":" + port + "...")
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		port = "8000"
		name, err := os.Hostname()
		if err != nil {
			fmt.Println(err)
		}
		http.Handle("/socket.io/", server)
		http.Handle("/", http.FileServer(http.Dir("./server/asset")))
		http.Handle("/payment", &socketServerHandler{Server: *server})

		log.Println("Serving at " + name + "...")
		log.Fatal(http.ListenAndServe(":"+port, nil))
	}
}
