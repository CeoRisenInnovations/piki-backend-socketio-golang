package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	log.Println("\nDo stuff BEFORE the tests!")
	exitVal := m.Run()
	log.Println("\nDo stuff AFTER the tests!")

	os.Exit(exitVal)
}

func TestServerUp(t *testing.T) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "ping")
	}

	req := httptest.NewRequest("GET", "localhost:", nil)
	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	fmt.Println(resp.StatusCode)
	fmt.Println(string(body))

	//ts := httptest.NewServer(http.FileServer(http.Dir("./asset")))
	//
	//defer ts.Close()
	//
	//res, err := http.Get(ts.URL)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//greeting, err := ioutil.ReadAll(res.Body)
	//
	//res.Body.Close()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//if status := res.StatusCode; status != http.StatusOK {
	//	t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	//}
	//fmt.Println(greeting)

	//req, err := http.NewRequest("GET", "/", nil)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//
	//rr := httptest.NewRecorder
	//handler := http.Handle("/", http.FileServer(http.Dir("./asset")))

}
